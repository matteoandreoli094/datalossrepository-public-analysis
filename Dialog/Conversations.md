
```diff
--- a/src/main/java/eu/siacs/conversations/ui/BlocklistActivity.java
+++ b/src/main/java/eu/siacs/conversations/ui/BlocklistActivity.java
@@ -1,6 +1,8 @@
 package eu.siacs.conversations.ui;
 
 import android.os.Bundle;
+import android.support.v4.app.Fragment;
+import android.support.v4.app.FragmentTransaction;
 import android.text.Editable;
 import android.view.Menu;
 import android.view.MenuItem;
@@ -62,6 +64,12 @@ protected void filterContacts(final String needle) {
 	}
 
 	protected void showEnterJidDialog() {
+		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
+		Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
+		if (prev != null) {
+			ft.remove(prev);
+		}
+		ft.addToBackStack(null);
 		EnterJidDialog dialog = EnterJidDialog.newInstance(
 				mKnownHosts, null,
 				getString(R.string.block_jabber_id), getString(R.string.block),
@@ -76,7 +84,7 @@ protected void showEnterJidDialog() {
 			return true;
 		});
 
-		dialog.show(getSupportFragmentManager(), "block_contact_dialog");
+		dialog.show(ft, "dialog");
 	}
 
 	protected void refreshUiReal() {
diff --git a/src/main/java/eu/siacs/conversations/ui/ChooseContactActivity.java b/src/main/java/eu/siacs/conversations/ui/ChooseContactActivity.java
index 42a1f1d75e..354aaadf32 100644
--- a/src/main/java/eu/siacs/conversations/ui/ChooseContactActivity.java
+++ b/src/main/java/eu/siacs/conversations/ui/ChooseContactActivity.java
@@ -6,6 +6,8 @@
 import android.os.Bundle;
 import android.support.annotation.NonNull;
 import android.support.annotation.StringRes;
+import android.support.v4.app.Fragment;
+import android.support.v4.app.FragmentTransaction;
 import android.support.v7.app.ActionBar;
 import android.view.ActionMode;
 import android.view.Menu;
@@ -230,6 +232,12 @@ public boolean onOptionsItemSelected(MenuItem item) {
 	}
 
 	protected void showEnterJidDialog(XmppUri uri) {
+		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
+		Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
+		if (prev != null) {
+			ft.remove(prev);
+		}
+		ft.addToBackStack(null);
 		Jid jid = uri == null ? null : uri.getJid();
 		EnterJidDialog dialog = EnterJidDialog.newInstance(
 				mKnownHosts,
@@ -256,7 +264,7 @@ protected void showEnterJidDialog(XmppUri uri) {
 			return true;
 		});
 
-		dialog.show(getSupportFragmentManager(), "enter_contact_dialog");
+		dialog.show(ft, "dialog");
 	}
 
 	@Override
diff --git a/src/main/java/eu/siacs/conversations/ui/CreateConferenceDialog.java b/src/main/java/eu/siacs/conversations/ui/CreateConferenceDialog.java
index 558478805e..7de13b39c6 100644
--- a/src/main/java/eu/siacs/conversations/ui/CreateConferenceDialog.java
+++ b/src/main/java/eu/siacs/conversations/ui/CreateConferenceDialog.java
@@ -1,6 +1,7 @@
 package eu.siacs.conversations.ui;
 
 import android.app.Dialog;
+import android.databinding.DataBindingUtil;
 import android.support.annotation.NonNull;
 import android.support.v4.app.DialogFragment;
 import android.content.Context;
@@ -15,6 +16,7 @@
 import java.util.List;
 
 import eu.siacs.conversations.R;
+import eu.siacs.conversations.databinding.CreateConferenceDialogBinding;
 
 public class CreateConferenceDialog extends DialogFragment {
 
@@ -40,16 +42,14 @@ public void onActivityCreated(Bundle savedInstanceState) {
     public Dialog onCreateDialog(Bundle savedInstanceState) {
         final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
         builder.setTitle(R.string.dialog_title_create_conference);
-        final View dialogView = getActivity().getLayoutInflater().inflate(R.layout.create_conference_dialog, null);
-        final Spinner spinner = dialogView.findViewById(R.id.account);
-        final EditText subject = dialogView.findViewById(R.id.subject);
+        CreateConferenceDialogBinding binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.create_conference_dialog, null, false);
         ArrayList<String> mActivatedAccounts = getArguments().getStringArrayList(ACCOUNTS_LIST_KEY);
-        StartConversationActivity.populateAccountSpinner(getActivity(), mActivatedAccounts, spinner);
-        builder.setView(dialogView);
+        StartConversationActivity.populateAccountSpinner(getActivity(), mActivatedAccounts, binding.account);
+        builder.setView(binding.getRoot());
         builder.setPositiveButton(R.string.choose_participants, new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
-                mListener.onCreateDialogPositiveClick(spinner, subject.getText().toString());
+                mListener.onCreateDialogPositiveClick(binding.account, binding.subject.getText().toString());
             }
         });
         builder.setNegativeButton(R.string.cancel, null);
diff --git a/src/main/java/eu/siacs/conversations/ui/EnterJidDialog.java b/src/main/java/eu/siacs/conversations/ui/EnterJidDialog.java
index 1a95d29888..1eb196244f 100644
--- a/src/main/java/eu/siacs/conversations/ui/EnterJidDialog.java
+++ b/src/main/java/eu/siacs/conversations/ui/EnterJidDialog.java
@@ -1,5 +1,6 @@
 package eu.siacs.conversations.ui;
 
+import android.databinding.DataBindingUtil;
 import android.support.annotation.NonNull;
 import android.support.v4.app.DialogFragment;
 import android.os.Bundle;
@@ -17,6 +18,7 @@
 
 import eu.siacs.conversations.Config;
 import eu.siacs.conversations.R;
+import eu.siacs.conversations.databinding.EnterJidDialogBinding;
 import eu.siacs.conversations.ui.adapter.KnownHostsAdapter;
 import eu.siacs.conversations.ui.util.DelayedHintHelper;
 import rocks.xmpp.addr.Jid;
@@ -61,59 +63,57 @@ public void onActivityCreated(Bundle savedInstanceState) {
 	public Dialog onCreateDialog(Bundle savedInstanceState) {
 		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
 		builder.setTitle(getArguments().getString(TITLE_KEY));
-		View dialogView = getActivity().getLayoutInflater().inflate(R.layout.enter_jid_dialog, null);
-		final Spinner spinner = dialogView.findViewById(R.id.account);
-		final AutoCompleteTextView jid = dialogView.findViewById(R.id.jid);
-		jid.setAdapter(new KnownHostsAdapter(getActivity(), R.layout.simple_list_item, (Collection<String>) getArguments().getSerializable(CONFERENCE_HOSTS_KEY)));
+		EnterJidDialogBinding binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.enter_jid_dialog, null, false);
+		binding.jid.setAdapter(new KnownHostsAdapter(getActivity(), R.layout.simple_list_item, (Collection<String>) getArguments().getSerializable(CONFERENCE_HOSTS_KEY)));
 		String prefilledJid = getArguments().getString(PREFILLED_JID_KEY);
 		if (prefilledJid != null) {
-			jid.append(prefilledJid);
+			binding.jid.append(prefilledJid);
 			if (!getArguments().getBoolean(ALLOW_EDIT_JID_KEY)) {
-				jid.setFocusable(false);
-				jid.setFocusableInTouchMode(false);
-				jid.setClickable(false);
-				jid.setCursorVisible(false);
+				binding.jid.setFocusable(false);
+				binding.jid.setFocusableInTouchMode(false);
+				binding.jid.setClickable(false);
+				binding.jid.setCursorVisible(false);
 			}
 		}
 
-		DelayedHintHelper.setHint(R.string.account_settings_example_jabber_id,jid);
+		DelayedHintHelper.setHint(R.string.account_settings_example_jabber_id, binding.jid);
 
 		String account = getArguments().getString(ACCOUNT_KEY);
 		if (account == null) {
-			StartConversationActivity.populateAccountSpinner(getActivity(), getArguments().getStringArrayList(ACCOUNTS_LIST_KEY), spinner);
+			StartConversationActivity.populateAccountSpinner(getActivity(), getArguments().getStringArrayList(ACCOUNTS_LIST_KEY), binding.account);
 		} else {
 			ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
 					R.layout.simple_list_item,
 					new String[] { account });
-			spinner.setEnabled(false);
+			binding.account.setEnabled(false);
 			adapter.setDropDownViewResource(R.layout.simple_list_item);
-			spinner.setAdapter(adapter);
+			binding.account.setAdapter(adapter);
 		}
 
-		builder.setView(dialogView);
+		builder.setView(binding.getRoot());
 		builder.setNegativeButton(R.string.cancel, null);
 		builder.setPositiveButton(getArguments().getString(POSITIVE_BUTTON_KEY), null);
 		AlertDialog dialog = builder.create();
 
 		View.OnClickListener dialogOnClick = v -> {
 			final Jid accountJid;
-			if (!spinner.isEnabled() && account == null) {
+			if (!binding.account.isEnabled() && account == null) {
 				return;
 			}
 			try {
 				if (Config.DOMAIN_LOCK != null) {
-					accountJid = Jid.of((String) spinner.getSelectedItem(), Config.DOMAIN_LOCK, null);
+					accountJid = Jid.of((String) binding.account.getSelectedItem(), Config.DOMAIN_LOCK, null);
 				} else {
-					accountJid = Jid.of((String) spinner.getSelectedItem());
+					accountJid = Jid.of((String) binding.account.getSelectedItem());
 				}
 			} catch (final IllegalArgumentException e) {
 				return;
 			}
 			final Jid contactJid;
 			try {
-				contactJid = Jid.of(jid.getText().toString());
+				contactJid = Jid.of(binding.jid.getText().toString());
 			} catch (final IllegalArgumentException e) {
-				jid.setError(getActivity().getString(R.string.invalid_jid));
+				binding.jid.setError(getActivity().getString(R.string.invalid_jid));
 				return;
 			}
 
@@ -123,7 +123,7 @@ public Dialog onCreateDialog(Bundle savedInstanceState) {
 						dialog.dismiss();
 					}
 				} catch(JidError error) {
-					jid.setError(error.toString());
+					binding.jid.setError(error.toString());
 				}
 			}
 		};
diff --git a/src/main/java/eu/siacs/conversations/ui/JoinConferenceDialog.java b/src/main/java/eu/siacs/conversations/ui/JoinConferenceDialog.java
index d8de5e8bb3..e38c4f3b8a 100644
--- a/src/main/java/eu/siacs/conversations/ui/JoinConferenceDialog.java
+++ b/src/main/java/eu/siacs/conversations/ui/JoinConferenceDialog.java
@@ -1,6 +1,7 @@
 package eu.siacs.conversations.ui;
 
 import android.app.Dialog;
+import android.databinding.DataBindingUtil;
 import android.support.annotation.NonNull;
 import android.support.v4.app.DialogFragment;
 import android.content.Context;
@@ -19,6 +20,7 @@
 import java.util.List;
 
 import eu.siacs.conversations.R;
+import eu.siacs.conversations.databinding.JoinConferenceDialogBinding;
 import eu.siacs.conversations.ui.adapter.KnownHostsAdapter;
 import eu.siacs.conversations.ui.util.DelayedHintHelper;
 
@@ -50,19 +52,15 @@ public void onActivityCreated(Bundle savedInstanceState) {
     public Dialog onCreateDialog(Bundle savedInstanceState) {
         final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
         builder.setTitle(R.string.dialog_title_join_conference);
-        final View dialogView = getActivity().getLayoutInflater().inflate(R.layout.join_conference_dialog, null);
-        final Spinner spinner = dialogView.findViewById(R.id.account);
-        final AutoCompleteTextView jid = dialogView.findViewById(R.id.jid);
-        DelayedHintHelper.setHint(R.string.conference_address_example, jid);
-        jid.setAdapter(new KnownHostsAdapter(getActivity(), R.layout.simple_list_item, (Collection<String>) getArguments().getSerializable(CONFERENCE_HOSTS_KEY)));
+        JoinConferenceDialogBinding binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.join_conference_dialog, null, false);
+        DelayedHintHelper.setHint(R.string.conference_address_example, binding.jid);
+        binding.jid.setAdapter(new KnownHostsAdapter(getActivity(), R.layout.simple_list_item, (Collection<String>) getArguments().getSerializable(CONFERENCE_HOSTS_KEY)));
         String prefilledJid = getArguments().getString(PREFILLED_JID_KEY);
         if (prefilledJid != null) {
-            jid.append(prefilledJid);
+            binding.jid.append(prefilledJid);
         }
-        final Checkable bookmarkCheckBox = (CheckBox) dialogView
-                .findViewById(R.id.bookmark);
-        StartConversationActivity.populateAccountSpinner(getActivity(), getArguments().getStringArrayList(ACCOUNTS_LIST_KEY), spinner);
-        builder.setView(dialogView);
+        StartConversationActivity.populateAccountSpinner(getActivity(), getArguments().getStringArrayList(ACCOUNTS_LIST_KEY), binding.account);
+        builder.setView(binding.getRoot());
         builder.setPositiveButton(R.string.join, null);
         builder.setNegativeButton(R.string.cancel, null);
         AlertDialog dialog = builder.create();
@@ -70,7 +68,7 @@ public Dialog onCreateDialog(Bundle savedInstanceState) {
         dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
-                mListener.onJoinDialogPositiveClick(dialog, spinner, jid, bookmarkCheckBox.isChecked());
+                mListener.onJoinDialogPositiveClick(dialog, binding.account, binding.jid, binding.bookmark.isChecked());
             }
         });
         return dialog;
diff --git a/src/main/java/eu/siacs/conversations/ui/StartConversationActivity.java b/src/main/java/eu/siacs/conversations/ui/StartConversationActivity.java
index 7c403e5918..6dbb8a1cdf 100644
--- a/src/main/java/eu/siacs/conversations/ui/StartConversationActivity.java
+++ b/src/main/java/eu/siacs/conversations/ui/StartConversationActivity.java
@@ -440,6 +440,12 @@ public void onClick(DialogInterface dialog, int which) {
 
 	@SuppressLint("InflateParams")
 	protected void showCreateContactDialog(final String prefilledJid, final Invite invite) {
+		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
+		Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
+		if (prev != null) {
+			ft.remove(prev);
+		}
+		ft.addToBackStack(null);
 		EnterJidDialog dialog = EnterJidDialog.newInstance(
 				mKnownHosts, mActivatedAccounts,
 				getString(R.string.dialog_title_create_contact), getString(R.string.create),
@@ -474,18 +480,30 @@ protected void showCreateContactDialog(final String prefilledJid, final Invite i
 				return true;
 			}
 		});
-		dialog.show(getSupportFragmentManager(), "create_contact_dialog");
+		dialog.show(ft, "dialog");
 	}
 
 	@SuppressLint("InflateParams")
 	protected void showJoinConferenceDialog(final String prefilledJid) {
-		JoinConferenceDialog dialog = JoinConferenceDialog.newInstance(prefilledJid, mActivatedAccounts, mKnownConferenceHosts);
-		dialog.show(getSupportFragmentManager(),"join_conference_dialog");
+		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
+		Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
+		if (prev != null) {
+			ft.remove(prev);
+		}
+		ft.addToBackStack(null);
+		JoinConferenceDialog joinConferenceFragment = JoinConferenceDialog.newInstance(prefilledJid, mActivatedAccounts, mKnownConferenceHosts);
+		joinConferenceFragment.show(ft, "dialog");
 	}
 
 	private void showCreateConferenceDialog() {
-		CreateConferenceDialog dialog = CreateConferenceDialog.newInstance(mActivatedAccounts);
-		dialog.show(getSupportFragmentManager(),"create_conference_dialog");
+		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
+		Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
+		if (prev != null) {
+			ft.remove(prev);
+		}
+		ft.addToBackStack(null);
+		CreateConferenceDialog createConferenceFragment = CreateConferenceDialog.newInstance(mActivatedAccounts);
+		createConferenceFragment.show(ft, "dialog");
 	}
 
 	private Account getSelectedAccount(Spinner spinner) {
diff --git a/src/main/res/layout/create_conference_dialog.xml b/src/main/res/layout/create_conference_dialog.xml
index 0fc3ec258d..f17250f32a 100644
--- a/src/main/res/layout/create_conference_dialog.xml
+++ b/src/main/res/layout/create_conference_dialog.xml
@@ -1,40 +1,42 @@
 <?xml version="1.0" encoding="utf-8"?>
-<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
-    android:layout_width="match_parent"
-    android:layout_height="match_parent"
-    android:orientation="vertical"
-    android:paddingBottom="?attr/dialog_vertical_padding"
-    android:paddingLeft="?attr/dialog_horizontal_padding"
-    android:paddingRight="?attr/dialog_horizontal_padding"
-    android:paddingTop="?attr/dialog_vertical_padding">
-    <TextView
+<layout xmlns:android="http://schemas.android.com/apk/res/android">
+    <LinearLayout
         android:layout_width="match_parent"
-        android:layout_height="wrap_content"
-        android:text="@string/your_account"
-        style="@style/InputLabel" />
-    <Spinner
-        android:id="@+id/account"
-        android:layout_width="fill_parent"
-        android:layout_height="wrap_content"/>
+        android:layout_height="match_parent"
+        android:orientation="vertical"
+        android:paddingBottom="?attr/dialog_vertical_padding"
+        android:paddingLeft="?attr/dialog_horizontal_padding"
+        android:paddingRight="?attr/dialog_horizontal_padding"
+        android:paddingTop="?attr/dialog_vertical_padding">
 
-    <View
-        android:focusable="true"
-        android:focusableInTouchMode="true"
-        android:layout_width="0dp"
-        android:layout_height="0dp"/>
-
-    <android.support.design.widget.TextInputLayout
-        android:layout_width="match_parent"
-        android:layout_height="wrap_content">
-
-        <android.support.design.widget.TextInputEditText
-            android:id="@+id/subject"
+        <TextView
             android:layout_width="match_parent"
             android:layout_height="wrap_content"
-            android:nextFocusUp="@+id/subject"
-            android:nextFocusDown="@+id/subject"
-            android:hint="@string/edit_subject_hint"/>
+            android:text="@string/your_account"
+            style="@style/InputLabel" />
 
-    </android.support.design.widget.TextInputLayout>
+        <Spinner
+            android:id="@+id/account"
+            android:layout_width="fill_parent"
+            android:layout_height="wrap_content"/>
+
+        <View
+            android:focusable="true"
+            android:focusableInTouchMode="true"
+            android:layout_width="0dp"
+            android:layout_height="0dp"/>
+
+        <android.support.design.widget.TextInputLayout
+            android:layout_width="match_parent"
+            android:layout_height="wrap_content">
 
-</LinearLayout>
+            <android.support.design.widget.TextInputEditText
+                android:id="@+id/subject"
+                android:layout_width="match_parent"
+                android:layout_height="wrap_content"
+                android:nextFocusUp="@+id/subject"
+                android:nextFocusDown="@+id/subject"
+                android:hint="@string/edit_subject_hint"/>
+        </android.support.design.widget.TextInputLayout>
+    </LinearLayout>
+</layout>
diff --git a/src/main/res/layout/enter_jid_dialog.xml b/src/main/res/layout/enter_jid_dialog.xml
index de178726cb..9137b054e9 100644
--- a/src/main/res/layout/enter_jid_dialog.xml
+++ b/src/main/res/layout/enter_jid_dialog.xml
@@ -1,35 +1,36 @@
 <?xml version="1.0" encoding="utf-8"?>
-<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
-              android:layout_width="match_parent"
-              android:layout_height="match_parent"
-              android:orientation="vertical"
-              android:paddingBottom="?attr/dialog_vertical_padding"
-              android:paddingLeft="?attr/dialog_horizontal_padding"
-              android:paddingRight="?attr/dialog_horizontal_padding"
-              android:paddingTop="?attr/dialog_vertical_padding">
-
-    <TextView
-        style="@style/InputLabel"
-        android:layout_width="wrap_content"
-        android:layout_height="wrap_content"
-        android:text="@string/your_account"/>
-
-    <Spinner
-        android:id="@+id/account"
-        android:layout_width="fill_parent"
-        android:layout_height="wrap_content"/>
-
-    <android.support.design.widget.TextInputLayout
-        android:id="@+id/account_jid_layout"
+<layout xmlns:android="http://schemas.android.com/apk/res/android">
+    <LinearLayout
         android:layout_width="match_parent"
-        android:layout_height="wrap_content"
-        android:hint="@string/account_settings_jabber_id">
+        android:layout_height="match_parent"
+        android:orientation="vertical"
+        android:paddingBottom="?attr/dialog_vertical_padding"
+        android:paddingLeft="?attr/dialog_horizontal_padding"
+        android:paddingRight="?attr/dialog_horizontal_padding"
+        android:paddingTop="?attr/dialog_vertical_padding">
 
-        <AutoCompleteTextView
-            android:id="@+id/jid"
+        <TextView
+            style="@style/InputLabel"
+            android:layout_width="wrap_content"
+            android:layout_height="wrap_content"
+            android:text="@string/your_account"/>
+
+        <Spinner
+            android:id="@+id/account"
             android:layout_width="fill_parent"
+            android:layout_height="wrap_content"/>
+
+        <android.support.design.widget.TextInputLayout
+            android:id="@+id/account_jid_layout"
+            android:layout_width="match_parent"
             android:layout_height="wrap_content"
-            android:inputType="textEmailAddress"/>
-    </android.support.design.widget.TextInputLayout>
+            android:hint="@string/account_settings_jabber_id">
 
-</LinearLayout>
\ No newline at end of file
+            <AutoCompleteTextView
+                android:id="@+id/jid"
+                android:layout_width="fill_parent"
+                android:layout_height="wrap_content"
+                android:inputType="textEmailAddress"/>
+        </android.support.design.widget.TextInputLayout>
+    </LinearLayout>
+</layout>
diff --git a/src/main/res/layout/join_conference_dialog.xml b/src/main/res/layout/join_conference_dialog.xml
index d89b95c39c..dfe8d30f97 100644
--- a/src/main/res/layout/join_conference_dialog.xml
+++ b/src/main/res/layout/join_conference_dialog.xml
@@ -1,43 +1,44 @@
 <?xml version="1.0" encoding="utf-8"?>
-<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
-              android:layout_width="match_parent"
-              android:layout_height="match_parent"
-              android:orientation="vertical"
-              android:paddingBottom="8dp"
-              android:paddingLeft="24dp"
-              android:paddingRight="24dp"
-              android:paddingTop="16dp">
-    <TextView
-        style="@style/InputLabel"
-        android:layout_width="wrap_content"
-        android:layout_height="wrap_content"
-        android:text="@string/your_account"/>
-
-    <Spinner
-        android:id="@+id/account"
-        android:layout_width="fill_parent"
-        android:layout_height="wrap_content" />
-
-    <android.support.design.widget.TextInputLayout
-        android:id="@+id/account_jid_layout"
+<layout xmlns:android="http://schemas.android.com/apk/res/android">
+    <LinearLayout
         android:layout_width="match_parent"
-        android:layout_height="wrap_content"
-        android:hint="@string/conference_address">
+        android:layout_height="match_parent"
+        android:orientation="vertical"
+        android:paddingBottom="8dp"
+        android:paddingLeft="24dp"
+        android:paddingRight="24dp"
+        android:paddingTop="16dp">
 
-        <AutoCompleteTextView
-            android:id="@+id/jid"
-            android:layout_width="fill_parent"
+        <TextView
+            style="@style/InputLabel"
+            android:layout_width="wrap_content"
             android:layout_height="wrap_content"
-            android:inputType="textEmailAddress"/>
-    </android.support.design.widget.TextInputLayout>
+            android:text="@string/your_account"/>
 
+        <Spinner
+            android:id="@+id/account"
+            android:layout_width="fill_parent"
+            android:layout_height="wrap_content" />
 
-    <CheckBox
-        android:id="@+id/bookmark"
-        android:layout_width="wrap_content"
-        android:layout_height="wrap_content"
-        android:layout_marginTop="8dp"
-        android:checked="true"
-        android:text="@string/save_as_bookmark"/>
+        <android.support.design.widget.TextInputLayout
+            android:id="@+id/account_jid_layout"
+            android:layout_width="match_parent"
+            android:layout_height="wrap_content"
+            android:hint="@string/conference_address">
+
+            <AutoCompleteTextView
+                android:id="@+id/jid"
+                android:layout_width="fill_parent"
+                android:layout_height="wrap_content"
+                android:inputType="textEmailAddress"/>
+        </android.support.design.widget.TextInputLayout>
 
-</LinearLayout>
\ No newline at end of file
+        <CheckBox
+            android:id="@+id/bookmark"
+            android:layout_width="wrap_content"
+            android:layout_height="wrap_content"
+            android:layout_marginTop="8dp"
+            android:checked="true"
+            android:text="@string/save_as_bookmark"/>
+    </LinearLayout>
+</layout>
```
