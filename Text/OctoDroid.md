```diff
 src/com/gh4a/activities/IssueEditActivity.java | 8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)

diff --git a/src/com/gh4a/activities/IssueEditActivity.java b/src/com/gh4a/activities/IssueEditActivity.java
index cc3595266..67320588a 100644
--- a/src/com/gh4a/activities/IssueEditActivity.java
+++ b/src/com/gh4a/activities/IssueEditActivity.java
@@ -216,15 +216,15 @@ public void onCreate(Bundle savedInstanceState) {
         mLabelContainer = findViewById(R.id.label_container);
 
         getSupportLoaderManager().initLoader(3, null, mIsCollaboratorCallback);
-        if (!isInEditMode()) {
+
+        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_KEY_ISSUE)) {
+            mEditIssue = (Issue) savedInstanceState.getSerializable(STATE_KEY_ISSUE);
+        } else if (!isInEditMode()) {
             getSupportLoaderManager().initLoader(4, null, mIssueTemplateCallback);
             mDescView.setEnabled(false);
             mDescWrapper.setHint(getString(R.string.issue_loading_template_hint));
         }
 
-        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_KEY_ISSUE)) {
-            mEditIssue = (Issue) savedInstanceState.getSerializable(STATE_KEY_ISSUE);
-        }
         if (mEditIssue == null) {
             mEditIssue = new Issue();
         }
         ```