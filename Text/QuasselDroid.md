```diff
.../quasseldroid/gui/LoginActivity.java       | 35 ++++++++++++++++++-
 1 file changed, 34 insertions(+), 1 deletion(-)

diff --git a/QuasselDroid/src/main/java/com/iskrembilen/quasseldroid/gui/LoginActivity.java b/QuasselDroid/src/main/java/com/iskrembilen/quasseldroid/gui/LoginActivity.java
index e42d24a7..75cff4ea 100644
--- a/QuasselDroid/src/main/java/com/iskrembilen/quasseldroid/gui/LoginActivity.java
+++ b/QuasselDroid/src/main/java/com/iskrembilen/quasseldroid/gui/LoginActivity.java
@@ -79,6 +79,10 @@
     private static final String TAG = LoginActivity.class.getSimpleName();
     public static final String PREFS_ACCOUNT = "AccountPreferences";
     public static final String PREFS_CORE = "coreSelection";
+    public static final String STORE_SELECTED_CORE = "SELECTED_CORE";
+    public static final String STORE_PASSWORD = "PASSWORD";
+    public static final String STORE_USERNAME = "USERNAME";
+    public static final String STORE_REMEMBER = "REMEMBER";
 
     private SharedPreferences settings;
     private QuasselDbHelper dbHelper;
@@ -91,6 +95,8 @@
     private EditText nameField;
     private EditText addressField;
 
+    private boolean hasLoadedFromSavedInstanceState = true;
+
     private String hashedCert;//ugly
     private int currentTheme;
 
@@ -98,7 +104,7 @@
      * Called when the activity is first created.
      */
     @Override
-    public void onCreate(Bundle savedInstanceState) {
+    public void onCreate(final Bundle savedInstanceState) {
         Log.d(TAG, "Creating activity");
         setTheme(ThemeUtil.theme);
         super.onCreate(savedInstanceState);
@@ -124,6 +130,19 @@ public void onItemSelected(AdapterView<?> parent, View view, int position, long
                     usernameField.setText(username);
                     passwordField.setText(password);
                     rememberMe.setChecked(true);
+
+                    if (!hasLoadedFromSavedInstanceState) {
+                        if (savedInstanceState.containsKey(STORE_PASSWORD)) {
+                            passwordField.setText(savedInstanceState.getString(STORE_PASSWORD));
+                        }
+                        if (savedInstanceState.containsKey(STORE_USERNAME)) {
+                            usernameField.setText(savedInstanceState.getString(STORE_USERNAME));
+                        }
+                        if(savedInstanceState.containsKey(STORE_REMEMBER)) {
+                            rememberMe.setChecked(savedInstanceState.getBoolean(STORE_REMEMBER));
+                        }
+                        hasLoadedFromSavedInstanceState = true;
+                    }
                 } else {
                     usernameField.setText("");
                     passwordField.setText("");
@@ -164,6 +183,20 @@ public void onClick(View view) {
 
         Button connect = (Button) findViewById(R.id.connect_button);
         connect.setOnClickListener(onConnect);
+
+        if (savedInstanceState != null && savedInstanceState.containsKey(STORE_SELECTED_CORE)) {
+            core.setSelection(savedInstanceState.getInt(STORE_SELECTED_CORE, -1));
+            hasLoadedFromSavedInstanceState = false;
+        }
+    }
+
+    @Override
+    protected void onSaveInstanceState(Bundle outState) {
+        super.onSaveInstanceState(outState);
+        outState.putInt(STORE_SELECTED_CORE, core.getSelectedItemPosition());
+        outState.putString(STORE_USERNAME, usernameField.getText().toString());
+        outState.putString(STORE_PASSWORD, passwordField.getText().toString());
+        outState.putBoolean(STORE_REMEMBER, rememberMe.isChecked());
     }
 ```
 