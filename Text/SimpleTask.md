```diff
 app/src/main/java/nl/mpcjanssen/simpletask/AddTask.kt | 5 ++++-
 1 file changed, 4 insertions(+), 1 deletion(-)

diff --git a/app/src/main/java/nl/mpcjanssen/simpletask/AddTask.kt b/app/src/main/java/nl/mpcjanssen/simpletask/AddTask.kt
index 8109b54af..bf3183855 100644
--- a/app/src/main/java/nl/mpcjanssen/simpletask/AddTask.kt
+++ b/app/src/main/java/nl/mpcjanssen/simpletask/AddTask.kt
@@ -108,7 +108,10 @@ class AddTask : ThemedActionBarActivity() {
                     ""
                 }
                 start_text = preFillString
-                textInputField.setText(preFillString)
+                // Avoid discarding changes on rotate
+                if (textInputField.text.isEmpty()) {
+                    textInputField.text = preFillString
+                }
                 // Listen to enter events, use IME_ACTION_NEXT for soft keyboards
                 // like Swype where ENTER keyCode is not generated.
 ```